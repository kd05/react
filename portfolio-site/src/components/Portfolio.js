import React from 'react';
import {Link} from "react-router-dom";

const Portfolio = () => {
    return (
        <div>
            <p>This is a portfolio page</p>
            <p>
                <Link to="/portfolio/1">Portfolio 1</Link>
                <Link to="/portfolio/2">Portfolio 2</Link>
            </p>
        </div>
    )
}

export default Portfolio;