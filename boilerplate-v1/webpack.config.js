//entry -> output
const path = require('path');

console.log('Dir path: ', __dirname);
module.exports = {
    entry: './src/app.js',
    output: {
        path : path.join(__dirname, 'public'),
        filename : 'bundle.js'
    },
    module:{
        rules:[
            {
                loader: 'babel-loader',
                test: /\.js$/,
                exclude: /node_modules/
            },
            {
                test: /\.s?css$/,
                use: [
                    // Creates `style` nodes from JS strings
                    'style-loader',
                    // Translates CSS into CommonJS
                    'css-loader',

                    // Behind the scene sass-loader will use node-sass to convert .scss to css
                    'sass-loader',
                ]
            }
        ]
    },
    devtool: 'cheap-module-eval-source-map',
    devServer: {
        contentBase: path.join(__dirname, 'public')
    }

};

// ############################
// #######    Read me   #######
// ############################

// # Babel-cli => It allows you to run babel through command line
// # Babel-core => It allows you to run babel from tools like web-pack. Basically babel
//                  converts the es6 or latest js functionality into older browser compatible code.
// # Babel-loader => It is a webpack plugin that allow us to teach webpack
//                  how to run babel when webpack sees certain files
//                  (it will use preset in .babelrc file)

// # devtool =>

// # css-loader => It is going to teach webpack how to take css and converted
//                  into a javascript representation of that css
// # style-loader => It takes the css that's in javascript and adds it to the DOM by
//                   injecting a style tag that will actually get our styles showing up in the browser


// # node-sass => It takes our scss/sass code and converts into regular CSS.
// # sass-loader => It is basically same as Babel-loader. Its going to
//                  allow us to import those scss files