<script src="https://code.jquery.com/jquery-3.5.1.js" ></script>
<script>
    jQuery(document).ready(function ($) {

        $('#ajaxBtn').click(function(){
            $.ajax("https://jsonplaceholder.typicode.com/todos/1",
                {
                    dataType: 'json', // type of response data
                    // timeout: 500,     // timeout milliseconds
                    success: function (data,status,xhr) {   // success callback function
                        console.log(data);
                        $('p').append('Todo ID: ' + data.id + ' | Title:' + data.title + ' | UserId: ' + data.userId);
                    },
                    error: function (jqXhr, textStatus, errorMessage) { // error callback
                        $('p').append('Error: ' + errorMessage);
                    }
                });
        });

    });
</script>


<input type="button" id="ajaxBtn" value="Send Ajax request" />
<p>
</p>