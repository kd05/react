
class Person {
    constructor(name = "Anonymous", age = 0){
        this.name = name;
        this.age = age;
    }

    getGreeting (){
        return `Hi, I am ${this.name}.`;
    }


    getDescription (){
        return `${this.name} is ${this.age} years old.`;
    }

}


class Traveler extends Person{

    constructor(name, age, homeLocation){
        super(name, age);
        this.homeLocation = homeLocation;
    }

    getGreeting() {
        let greeting = super.getGreeting();
        if (this.homeLocation){
            greeting += ` I'm visiting from ${this.homeLocation}`;
        }
        return greeting;
    }

}



const me = new Traveler("krunal" , 30,'Newmarket');
console.log(me, me.getGreeting());


const other = new Traveler("krish" , 23, );
console.log(other, other.getGreeting());