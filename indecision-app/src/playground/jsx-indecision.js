
const app = {
    title: "Indecision App",
    subtitle: "The tag line....",
    options: []
};

const onRemoveAll = () => {
    app.options = [];
    render();
    console.log(test);
}

const onSubmitForm = (e) => {
    e.preventDefault();
    const option = e.target.options.value;
    if(option){
        app.options.push(option);
        e.target.options.value = "";
        render();
    }
}

const onMakeDecision = () => {
    const randNum = Math.floor(Math.random() * app.options.length);
    const option = app.options[randNum];
    alert(option);
}

const appRoot = document.querySelector("#app");

const render = () => {
    const template =
        <div>
            <h1>{app.title}</h1>
            {app.subtitle && <p>{app.subtitle}</p>}
            <p>{app.options.length > 0 ? "Here are your options" : "No options"}</p>

            <button disabled={ (app.options.length === 0) } onClick={onMakeDecision}>What should I do?</button>
            <button onClick={onRemoveAll}>Remove All</button>

            <ol>
                {
                    app.options.map(number =>  <li key={number}> {number}</li> )
                }
            </ol>

            <form onSubmit={onSubmitForm}>
                <input type="text" name="options"/>
                <button>Add Item</button>
            </form>

        </div>
    ;

    ReactDOM.render(template, appRoot);
}


render();
