

function firstName(fullName) {
    return fullName.split(' ')[0];
}

console.log(firstName('krunal patel'));



const firstNameArrow = (fullName) =>  fullName.split(' ')[0];

console.log(firstNameArrow('chintu patel'));



const multiplier = {
    numbers : [2,5,7,8,9],
    multiplyBy : 10,
    multiply(){ return this.numbers.map( num => num * this.multiplyBy ) }
}
console.log(multiplier.multiply());





