class VisibilityToggle extends React.Component{

    constructor(props) {
        super(props);
        this.handleToggleVisibility = this.handleToggleVisibility.bind(this);
        this.state = {
            visibility : false
        }
    }
    handleToggleVisibility(){
        this.setState((prevState) => {
            return {
                visibility: !prevState.visibility
            }
        })
    }
    render(){
        const visibility = this.state.visibility;
        return(
            <div>
               <h1> Visibility Toggle</h1>
                <button onClick={this.handleToggleVisibility}> { (visibility) ? "Hide Details" : "Show Details" }</button>
                {visibility && <p>This is a toggle data for visibility.</p>}
            </div>
        );
    }
}

ReactDOM.render(<VisibilityToggle/>, document.getElementById('app'));




// let show = true;
//
// const updateToggle = () => {
//     show = !show;
//     render();
// }
//
// const appRoot = document.querySelector("#app");
//
//
// const render = () => {
//     const template = (
//         <div>
//             <h1>Visibility Toggle</h1>
//             <button onClick={updateToggle}> { (show) ? "Hide Details" : "Show Details" }</button>
//             {(show) && <p>Hey this is a test detail for the toggle example.</p>}
//         </div>
//     )
//     ReactDOM.render(template, appRoot);
// }
//
// render();