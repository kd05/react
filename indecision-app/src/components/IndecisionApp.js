import React from "react";
import Header from "./Header";
import Action from "./Action";
import Options from "./Options";
import AddOption from "./AddOption";
import OptionModal from "./OptionModal";

class IndecisionApp extends React.Component{

    state = {
        options : [],
        selectedOption : undefined
    }
    handelClearSelectedOptions = () => {
        this.setState(() => ({
          selectedOption: undefined
        }))
    }
    handleDeleteOptions = () => {
        this.setState(() => ({ options : []}) );
    }
    handlePick = () => {
        const randomNum = Math.floor(Math.random() * this.state.options.length);
        const option = this.state.options[randomNum];
        this.setState(() => ({
            selectedOption: option
        }));
        // alert(option);
    }
    handleAddOption = (option) => {
        if(!option){
            return "Please enter valid Option !"
        } else if (this.state.options.indexOf(option) != -1){
            return "This option already exists"
        }
        // this.setState((prevState) => {
        //    return {
        //        options: [...prevState.options, option]
        //    }
        // });
        this.setState((prevState) => ({ options: [...prevState.options, option] }));
    }

    handleDeleteOption = (optionToRemove) => {
        this.setState((prevState) => {
            return {
                options: prevState.options.filter((option) =>  option !== optionToRemove)
            };
        });
    }





    componentDidMount(){
        // if json data is valid inside the localStorage
        try{
            const json = localStorage.getItem('options');
            const options = JSON.parse(json);
            if(options){
                this.setState(() => ({options: options}));
            }
        }
        catch (e) {
            // Do Nothing
        }
    }
    componentDidUpdate(prevProps, prevState){

        if(prevState.options.length !== this.state.options.length){
            const json = JSON.stringify(this.state.options);
            localStorage.setItem('options', json);
        }
    }

    componentWillUnmount(){
        console.log('componentWillUnmount');
    }

    render(){
        const subtitle = "Put your life in hands of computers.";
        return(
            <div>
                <Header subtitle={subtitle}/>
                <div className="container">
                    <Action
                        hasOptions={this.state.options.length > 0}
                        handlePick={this.handlePick}
                    />

                    <div className="widget">
                        <Options
                            options={this.state.options}
                            handleDeleteOptions={this.handleDeleteOptions}
                            handleDeleteOption={this.handleDeleteOption}
                        />
                        <AddOption
                            handleAddOption={this.handleAddOption}
                        />
                    </div>

                </div>

                <OptionModal
                    selectedOption={this.state.selectedOption}
                    handelClearSelectedOptions={this.handelClearSelectedOptions}
                />
            </div>
        )
    }
}


export default IndecisionApp;