console.log("Destructuring...");

// const person = {
//     name : 'Krish',
//     age: 32,
//     location : {
//         city : 'Newmarket',
//         temp : 23
//     }
// }
//
// console.log(`${person.name} is ${person.age}`);


const book = {
    title : 'Ego is the Enemy',
     author: "Ryan Holiday",
    publisher: {
        name : 'Penguin'
    }
};

const { name: publisherName = 'Self-Published' } = book.publisher;
console.log(publisherName);



const item = ['Coffee' , "$2.00",  "$2.50",  "$2.90"];

const [product , , price , ] = item;
console.log(`${product} is ${price}`);