import React from 'react';
import { BrowserRouter as Router,  Route } from 'react-router-dom';
import Header from './components/layout/header';
import Todos from './components/Todos';
import AddTodo from './components/AddTodo';
import About from './components/pages/About';
// import uuid from 'react-uuid';
import axios from 'axios';

import './App.css';

class App extends React.Component {

  constructor() {
    super();
    this.state = {
      todos:[
      ]
    };
  }


  componentDidMount() {
      axios.get('https://jsonplaceholder.typicode.com/todos?_limit=6')
          .then(res => {
             this.setState({
                 todos: res.data
             })
          });
  }


  markComplete  = (id) => {
    this.setState({
          todos : this.state.todos.map(todo => {
             if(id == todo.id) { todo.completed = !todo.completed; }
             return todo;
          })
        })
  }


  deleteTodo = (id) => {
    console.log('Delete Function',id);
      axios.delete(`https://jsonplaceholder.typicode.com/todos/${id}`)
          .then(res => {
              this.setState({
                  todos : this.state.todos.filter(todo => {
                      if(id != todo.id) return true;
                  })
              })
          });
  }


  addTodos = (title) => {

      axios.post('https://jsonplaceholder.typicode.com/todos',{
          title,
          completed: false
      })
      .then(res => {
          console.log(res.data);
          this.setState({
              todos: [...this.state.todos, res.data]
          })
      });
  }


  render() {
    // console.log(this.state.todos);
    return (
        <Router>
            <div className="App">
              <div className='container'>
                <Header />

                <Route exact path='/' render={props => (
                    <React.Fragment>
                        <AddTodo addTodos={this.addTodos} />
                        <Todos
                            todos={this.state.todos}
                            markComplete={this.markComplete}
                            deleteTodo={this.deleteTodo}
                        />
                    </React.Fragment>
                )}/>


                <Route path='/about' component={About}/>


              </div>
            </div>
        </Router>
    );
  }


}

export default App;
